import simplematrixbotlib as botlib
import json
import os
import sys
from chat import Chat

if not os.path.isdir("config"):
    os.mkdir("config")

active_chats = {}

possible_config_files = ["../config/config.json", "config/config.json"]
config_files = list(filter(lambda x: x[0], (map(lambda x: (os.path.isfile(x), x), possible_config_files))))
if len(config_files) > 0:
    config_file = config_files[0][1]
    with open(config_file) as json_file:
        appConfig = json.load(json_file)
else:
    print(f"No config file found!")
    sys.exit(1)

config = botlib.Config()
config.encryption_enabled = False
config.emoji_verify = False
config.ignore_unverified_devices = True
config.store_path = 'config/crypto_store/'
config.join_on_invite = True
config.allowlist = appConfig['user_allowlist']
config.blocklist = appConfig['user_blocklist']
creds = botlib.Creds(homeserver=appConfig['matrix']['homeserver_url'],
                     username=appConfig['matrix']['user'],
                     password=appConfig['matrix']['password'],
                     session_stored_file="config/session.txt")
bot = botlib.Bot(creds, config)


def update_verified_devices(room):
    roomUsers = bot.async_client.room_devices(room.room_id)
    for roomUser in roomUsers.items():
        for roomDevice in roomUser[1].items():
            olmDevice = roomDevice[1]
            if not olmDevice.verified:
                bot.async_client.verify_device(olmDevice)


@bot.listener.on_message_event
async def on_new_message(room, event):
    match = botlib.MessageMatch(room, event, bot)
    if match.is_not_from_this_bot():
        print(f"Received message {event.body} from {event.sender} in room {room.room_id}")

        if 'm.relates_to' not in event.source['content']:
            # New Message -> Start new chat/thread!
            print(f"  -> start of a new thread")
            thread_event_id = event.event_id
            options = {
                "model": appConfig['model']
            }
            chat = Chat(appConfig['mistral_api_key'], event_id=event.event_id, options=options)
            active_chats[event.event_id] = chat
        else:
            thread_event_id = event.source['content']['m.relates_to']['event_id']
            print(f"  -> refers to event_id {thread_event_id}")

            # Message was reply to existing chat => try to get handle to chat
            if thread_event_id in active_chats.keys():
                chat = active_chats[thread_event_id]
                print(f"  -> found chat instance -> reusing")
            else:
                # Don't have a handle on the chat anymore =>
                print(f"  -> chat instance not found -> send error message")
                await reply_in_citation(room.room_id, thread_event_id, "Chat is no longer available. Please start a new thread.")
                return

        print(f"  -> using chat: {chat.event_id} for thread_event_id {thread_event_id}")

        await bot.async_client.room_typing(room_id=room.room_id, typing_state=True)

        reply = chat.say(event.body)
        print(f"  -> replying to user")
        await reply_in_thread(room_id=room.room_id, event_id=chat.event_id, message=reply)

        await bot.async_client.room_typing(room_id=room.room_id, typing_state=False)


async def reply_in_thread(room_id, event_id, message):
    content = {
        "body": f"{message}",
        "msgtype": "m.text",
        "m.relates_to": {
            "rel_type": "m.thread",
            "event_id": event_id
        }
    }
    await bot.async_client.room_send(room_id, message_type="m.room.message", content=content)


async def reply_in_citation(room_id, event_id, message):
    content = {
        "body": f"{message}",
        "msgtype": "m.text",
        "m.in_reply_to": {
            "event_id": event_id
        }
    }
    await bot.async_client.room_send(room_id, message_type="m.room.message", content=content)


bot.run()
