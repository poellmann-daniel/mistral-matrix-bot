from mistralai.client import MistralClient
from mistralai.models.chat_completion import ChatMessage
import uuid


class Message:
    def __init__(self, role, message):
        self.role = role
        self.message = message


class Chat:

    def __init__(self, api_key, event_id=None, options=None):
        if event_id is None:
            self.event_id = str(uuid.uuid4())
        else:
            self.event_id = event_id
        if options is None:
            self.options = {}
        else:
            self.options = options

        if options is None or 'model' not in options:
            self.model = "mistral-large-latest"
        else:
            self.model = options['model']

        self.history = []
        self.client = MistralClient(api_key=api_key)

    def say(self, message):
        try:
            self.history.append(Message("user", message))
            chat_response = self.client.chat(
                model=self.model,
                messages=list(map(lambda m: ChatMessage(role=m.role, content=m.message), self.history)),
            )

            response = ""
            for choice in chat_response.choices:
                response += choice.message.content

            self.history.append(Message("assistant", response))

        except Exception as e:
            response = f"There was an issue with calling the API.\n(Error: {e})"

        return response
